# Editoria BookSprints

This is the BookSprints flavor of the Editoria application.This favor is a customization of the vanilla flavor of Editoria and contains functionality tailored for the BookSprints.  

This application is being developed by the [Coko Foundation](https://coko.foundation/). For more information, visit the project's [website](https://editoria.pub/) or our [chat channel](https://mattermost.coko.foundation/coko/channels/editoria).  
For the editor that Editoria uses, see its related project [Wax](https://gitlab.coko.foundation/wax/wax).  

For an updated RoadMap please see the ReadMe at [https://gitlab.coko.foundation/editoria/editoria](https://gitlab.coko.foundation/editoria/editoria)


## Get up and running  

Get your copy of the repository.  
```sh
git clone https://gitlab.coko.foundation/editoria/booksprints.git
cd booksprints
```

Make sure you use an LTS version of node `node = 12.16.1`.

### nvm
To determine which version of Node you are running type `node -v`.
If the version is not 12.16.1 you will need to use nvm to prescribe a specific node version. Installation of nvm is covered here https://github.com/creationix/nvm#installation

Once nvm is installed use the command `nvm install 12`

For further information on how to use nvm see https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/

### Install yarn
```sh
npm install -g yarn
```

You should also have `yarn 1.19.1` or higher installed (out-of-date versions may cause installation errors).

### Install Docker and docker-compose
Instalation instructions can be found:
* https://docs.docker.com/install/
* https://docs.docker.com/compose/install/

Prefer latest stable version of docker (18.x.x) and docker-compose (1.2x.x)

### Install Dependencies
Go to the root folder of your cloned Editoria and run `yarn`  

Create environment files for each profile of the application under `editoria-vanilla/config`.  
eg. `editoria-vanilla/config/development.env`

Within your environment files, export the variables you want:
```sh
export PUBSWEET_SECRET='' (*) (**)
export POSTGRES_USER='' (*) (***) (used by both docker-compose.yml and pubsweet server)
export POSTGRES_PASSWORD='' (*) (***) (by from both docker-compose.yml and pubsweet server)
export POSTGRES_HOST='' (-)
export POSTGRES_DB='' (*) (***) (used by both docker-compose.yml and pubsweet server)
export POSTGRES_PORT='' (*) (***) (used by both docker-compose.yml and pubsweet server)
export LANGUAGE_PORT='8090'
export LANGUAGE_ENDPOINT='http://localhost'
export SERVER_PORT='' (**)
export MAILER_USER='' (*) (**)
export MAILER_PASSWORD='' (*) (**)
export MAILER_SENDER='' (*) (**)
export MAILER_HOSTNAME='' (*) (**)
export PASSWORD_RESET_URL='' (*) (**)
export PASSWORD_RESET_SENDER='' (*) (**)
export NODE_ENV='' (**)
```

(*)Required for the application to be functional

(-) Optional

(**) This key-value pairs could be either declared as env variables or either in the corresponding config file e.g. `local-development.json`, `development.json`, etc

(***) These fields should by any means exist in the env source file for the correct initialization of the docker container which holds the database of the application

Import the environment variables into the current shell session:
```sh
source <your-env-file>
```

Get the database docker container up and running:  
```sh
yarn start:postgres
```

Open a new terminal session and source again your environment file. Create a new database for the app by running `yarn setupdb` and follow the CLI to create also the admin user (this will happen once uppon your first configuration of the app)

When your db is ready you can start the server:
`yarn server`

When the server boots succesfully open a new terminal session, source again your environment file and run `yarn start:services` which will initialise and start all the additional required Editoria's services.

At this point you are good to go and use the app

If for some reason want to reset your app's db you could run:
```sh
yarn resetdb
```

If you want to clean your docker cache and containers e.g. changes occur in either env variables, docker-compose.yml, development.env, production.env, etc.:
```sh
docker-compose down
docker-compose rm -fv
rm -rf database
```

The above action will require you to run `yarn setupdb` again and also ALL YOUR DATA WILL BE LOST

IT IS REALLY IMPORTANT TO MAKE SURE THAT YOUR ENV FILE IS SOURCED BEFORE RUNNING ANY OF THE FOLLOWING:
`yarn start:postgres`
`yarn start:services`
`yarn setupdb`
`yarn server`

## Developer info

see also the [Pubsweet wiki](https://gitlab.coko.foundation/pubsweet/pubsweet/wikis/home) for developer notes.

## FAQ
### I'm getting user errors running `yarn start:postgres` or `yarn server`

It's crucial to use the same user when installing Editoria and running the Editoria database services. These commands are:
* Running `yarn` from Editoria's root directory. This installs Editoria and its dependencies
* Running `yarn start:postgres` for the first time sets up a database to use with Editoria. This configures a database that expects that the same user that is running this command has also run `yarn` from Editoria's root directory.

If you see user errors running `yarn start:postgres` or `yarn server`, your best bet is to clear the existing database  as well as your node_modules and start installing the app from the beginnig.

First, delete all the Docker data:
```
docker-compose down
docker-compose rm -fv
rm -rf database
rm -rf node_modules
```

Then, follow the steps for a clean install.

### When running `yarn start:postgres`, I get a `Bind for 0.0.0.0:5432 failed: port is already allocated` error

Something (probably postgres) is already running on the port that the Docker database services try to use (5432). Solution for Ubuntu:
1. `lsof -i tcp:5432`: lists the processes running on port 5432
2. `kill -9 {PID}`: kills (gracelessly) the process. Get the PID from the output of the above step.

This should free up the port so the Docker database services can run on it.

### I've made changes on my `<profile>.env` file, how can these changes be applied?

To be sure that your changes in `<profile>.env` are registered, you need to reset your docker containers and source `<your-env-file>`. To do so, you can follow these steps:

* Kill any running instances of Editoria app

* On editoria-app root folder perform:

```
docker-compose down
docker-compose rm -fv
```


  `docker-compose down` will unmount the docker container

  `docker-compose rm -fv` will remove the container and it's anonymous volumes.   

  `rm -rf database` will delete the content from your database

* Now you could run `source <your-env-file>` and start again the services and server

### Which are the absolute required key-value pairs for an env file?

* POSTGRES_USER
* POSTGRES_PASSWORD
* POSTGRES_DB
* POSTGRES_PORT

These values are needed in order the docker container which hosts the PostrgesDB of the application to be initialised correctly.


### How can I access the epub file?

EPUB files are created and stored in the `uploads` directory (`editoria/packages/editoria-app/uploads`) every time the book is exported with the "EXPORT BOOK" button on the top right of the book. Be sure to sort by created date to ensure you're getting the most recent file.

### Does the HTML out of Editoria support accessibility including the use of Alt tags?
We are working with Benetech to fully understand and plan for accessibility. This development is on our development roadmap.

### Does Editoria support multiple languages?
Yes. Editoria supports any language supported by the user’s browser. Editoria also supports special characters. This said, language support is an area that needs thorough testing for the many edge cases and rare cases that exist. This is an ideal opportunity for a community member to show leadership and help organize and optimize.

### Does Editoria include an asset manager for images and multimedia files that may need to be inserted into text?
This is on our development roadmap.

### Can Editoria integrate with other tools and services?
Yes. Editoria’s architecture is all API-orientated and can be easily extended for third party service integration. For more information, visit https://editoria.pub
  
### Can notes be moved to backmatter (rather than footnotes)?
At this moment no, but it is on the Editoria roadmap. Options will include same page, back of book, or margin notes.

### What’s the cost to use Editoria?
Using the code to create an instance of Editoria truly is free. Our hope is that organizations that find it useful, will contribute the customizations and additional development code back so that others can use it. We also hope that adopters will help organize and attend community gatherings and participate in discussion and problem solving with the community.

### Does Editoria generate and export EPUB3?
Yes, currently however it is not available via the user interface. This is on our development roadmap.

### Can Editoria export BITS XML (or other) for chapter files and books?
It can. The first conversion is from .docx to HTML, and from there, it’s up to presses to decide what to do with the highly structured, source HTML.

### Can I use Editoria for journals workflow?
It’s possible, but would not be ideal. Coko has developed an open-source tool that is optimized for journals workflow, called xPub. xPub, like Editoria, is modular, so that organizations can develop their own non-hardcoded workflows, mixing and matching modules that other organizations have developed and shared, or create and integrate their own. More at https://coko.foundation/use-cases/

### How do my .docx filenames affect how they upload?
Using the "Upload Word Files" button, you can upload multiple .docx files with one click. A few file naming conventions provide useful controls for how the Word files are uploaded:
* .docx files that begin with "a" go into Frontmatter
* .docx files that begin with "w" go into Backmatter
* files that start with any other letters go into the Body

Additionally:
* By default, files in the Body are regular, numbered chapters. Frontmatter and backmatter components are always unnumbered.
* a "00" anywhere in filename will make it an unnumbered chapter (only in Body)
* "pt0" anywhere in filename will upload the .docx as a Part (only in Body)