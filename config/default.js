const path = require('path')

const components = require('./components')
const bookBuilder = require('./modules/book-builder')
const winston = require('winston')
// const authsomeMode = require('./modules/authsome')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
    }),
  ],
})

module.exports = {
  authsome: {
    mode: require.resolve('./modules/authsome'),
    teams: {
      productionEditor: {
        name: 'Production Editor',
        role: 'productionEditor',
        color: {
          addition: '#0c457d',
          deletion: '#0c457d',
        },
        weight: 1,
      },
      author: {
        name: 'Author',
        role: 'author',
        color: {
          addition: '#e8702a',
          deletion: '#e8702a',
        },
        weight: 2,
      },
    },
  },
  bookBuilder,
  epub: {
    fontsPath: '/uploads/fonts',
  },
  'password-reset': {
    url: 'http://localhost:3000/password-reset',
    sender: 'dev@example.com',
  },
  mailer: {
    from: 'dev@example.com',
    path: path.join(__dirname, 'mailer'),
  },
  publicKeys: [
    'authsome',
    'bookBuilder',
    'pubsweet',
    'pubsweet-client',
    'pubsweet-server',
    'validations',
    'wax',
  ],
  pubsweet: {
    components,
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    'login-redirect': '/',
    navigation: 'app/components/Navigation/Navigation.jsx',
    routes: 'app/routes.jsx',
    theme: 'ThemeEditoria',
    converter: 'ucp',
  },
  'pubsweet-server': {
    db: {},
    enableExperimentalGraphql: true,
    graphiql: true,
    tokenExpiresIn: '360 days',
    sse: true,
    logger,
    port: 3000,
    uploads: 'uploads',
    pool: { min: 0, max: 10, idleTimeoutMillis: 1000 },
  },
  templates: ['Atla'],
  wax: {
    layout: 'editoria',
    lockWhenEditing: true,
    theme: 'editoria',
    autoSave: true,
    frontmatter: {
      default: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
      component: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
    },
    body: {
      default: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
      part: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
      chapter: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
      unnumbered: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
    },
    backmatter: {
      default: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
      component: {
        menus: {
          topToolBar: 'booksprintTopMenu',
          sideToolBar: 'booksprintSideMenu',
          overlay: 'booksprintOverlay',
        },
      },
    },
  },
  schema: {},
  validations: path.join(__dirname, 'modules', 'validations'),
}
