// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const credentials = require('./credentials')

Cypress.Commands.add('login', role => {
  const { username, password } = credentials[role]

  const loginUserMutation = `mutation{
    loginUser (input: {
      username: "${username}",
      password: "${password}"
  }) {
      token
    }
  }`

  window.localStorage.removeItem('token')

  cy.request({
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: { query: loginUserMutation },
  }).then(response => {
    const { token } = response.body.data.loginUser
    window.localStorage.setItem('token', token)
  })
})

Cypress.Commands.add('getCollections', () => {
  const token = window.localStorage.getItem('token')
  const getCollectionsQuery = `query(
    $ascending: Boolean = true
    $archived: Boolean = false
    $sortKey: String = "title"
  ) {
    getBookCollections {
      id
      title
      books(ascending: $ascending, sortKey: $sortKey, archived: $archived) {
        id
        title
        publicationDate
        isPublished
        archived
        authors {
          username
          givenName
          surname
        }
      }
    }
  }
`

  return cy.request({
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      authorization: token ? `Bearer ${token}` : '',
    },
    body: { query: getCollectionsQuery },
  })
})
